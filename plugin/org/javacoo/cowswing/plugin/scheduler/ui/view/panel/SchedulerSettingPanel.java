/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.scheduler.ui.view.panel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.annotation.Resource;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.javacoo.cowswing.core.cache.ICowSwingCacheManager;
import org.javacoo.cowswing.core.cache.support.CacheKeyConstant;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleBean;
import org.javacoo.cowswing.plugin.scheduler.service.beans.ScheduleTaskBean;
import org.javacoo.cowswing.plugin.scheduler.ui.model.RuleComboBoxModel;
import org.javacoo.cowswing.ui.listener.IntegerVerifier;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;
import org.springframework.stereotype.Component;

/**
 * 定时任务设置面板
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2013-5-5 下午4:38:00
 * @version 1.0
 */
@Component("schedulerSettingPanel")
public class SchedulerSettingPanel extends AbstractContentPanel<ScheduleTaskBean>{

	private static final long serialVersionUID = 1L;
	/**缓存管理*/
	@Resource(name="cowSwingCacheManager")
	private ICowSwingCacheManager crawlerCacheManager;
	/**定时任务名称输入框*/
	private javax.swing.JTextField nameField;
	/**定时任务名称标签*/
	private javax.swing.JLabel nameLabel;
	/**定时任务关联标签*/
	private javax.swing.JLabel taskLabel;
	/**定时任务关联下拉*/
	private JComboBox taskCombo;
	/***定时任务默认ComboBoxModel*/
	private DefaultComboBoxModel taskComboComboBoxModel;
	/**定时任务表达式输入框*/
	private javax.swing.JTextField expressionField;
	/**定时任务表达式标签*/
	private javax.swing.JLabel expressionLabel;
	/**周期名称标签*/
	private javax.swing.JLabel weekLabel;
	/**星期一*/
	private javax.swing.JCheckBox mondayCheckBox;
	/**星期二*/
	private javax.swing.JCheckBox tuesdayCheckBox;
	/**星期三*/
	private javax.swing.JCheckBox wednesdayCheckBox;
	/**星期四*/
	private javax.swing.JCheckBox thursdayCheckBox;
	/**星期五*/
	private javax.swing.JCheckBox fridayCheckBox;
	/**星期六*/
	private javax.swing.JCheckBox saturdayCheckBox;
	/**星期天*/
	private javax.swing.JCheckBox sundayCheckBox;
	
	/**时间名称标签*/
	private javax.swing.JLabel timeLabel;
	/**小时名称标签*/
	private javax.swing.JLabel hourLabel;
	/**时间关联下拉*/
	private JComboBox timeCombo;
	/**分钟名称标签*/
	private javax.swing.JLabel minuteLabel;
	/**分钟输入框*/
	private javax.swing.JTextField minuteField;
	/**秒名称标签*/
	private javax.swing.JLabel secondLabel;
	/**秒输入框*/
	private javax.swing.JTextField secondField;
	
	/**周期值*/
	private String week = "";
	/**服务名称,与spring配置一致*/
	private String serviceName = "schedulerRuleSvc";
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#populateData()
	 */
	@Override
	protected ScheduleTaskBean populateData() {
		ScheduleTaskBean schedulerBean = new ScheduleTaskBean();
		schedulerBean.setName(nameField.getText());
		if(null != taskCombo.getSelectedItem()){
			CrawlerRuleBean crawlerRuleBean = (CrawlerRuleBean) taskCombo.getSelectedItem();
			schedulerBean.setAssociateId(crawlerRuleBean.getRuleId());
		}else{
			JOptionPane.showMessageDialog(null, LanguageLoader.getString("Scheduler.isNotBlank"), LanguageLoader.getString("Common.alertTitle"), JOptionPane.CLOSED_OPTION);
			return null;
		}
		if(StringUtils.isBlank(expressionField.getText()) 
				&& StringUtils.isBlank(week) 
				&& null == timeCombo.getSelectedItem() 
				&& StringUtils.isBlank(minuteField.getText()) 
				&& StringUtils.isBlank(secondField.getText()) ){
			JOptionPane.showMessageDialog(null, LanguageLoader.getString("Scheduler.timeExp.isNotBlank"), LanguageLoader.getString("Common.alertTitle"), JOptionPane.CLOSED_OPTION);
			return null;
		}
		schedulerBean.setServiceName(serviceName);
		schedulerBean.setWeek(week);
		schedulerBean.setExpression(expressionField.getText());
		if(null != timeCombo.getSelectedItem()){
			schedulerBean.setHour(timeCombo.getSelectedItem().toString());
		}
		schedulerBean.setMinute(minuteField.getText());
		schedulerBean.setSecond(secondField.getText());
		return schedulerBean;
	}

	
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#initComponents()
	 */
	@Override
	protected void initComponents() {
		useGridBagLayout();
		
		nameLabel = new javax.swing.JLabel(LanguageLoader.getString("Scheduler.name"));
		addCmpToGridBag(nameLabel,new GBC(0,0));
		nameField = new javax.swing.JTextField();
		addCmpToGridBag(nameField,new GBC(1,0));
		
		taskLabel = new javax.swing.JLabel(LanguageLoader.getString("Scheduler.task"));
		addCmpToGridBag(taskLabel,new GBC(0,1));
		taskComboComboBoxModel = new DefaultComboBoxModel();
		taskCombo = new JComboBox(taskComboComboBoxModel);
		addCmpToGridBag(taskCombo,new GBC(1,1));
		
		expressionLabel = new javax.swing.JLabel(LanguageLoader.getString("Scheduler.expression"));
		addCmpToGridBag(expressionLabel,new GBC(0,2));
		expressionField = new javax.swing.JTextField();
		addCmpToGridBag(expressionField,new GBC(1,2));
		
		weekLabel = new javax.swing.JLabel(LanguageLoader.getString("Scheduler.week"));
		addCmpToGridBag(weekLabel,new GBC(0,3));
		
		mondayCheckBox = new javax.swing.JCheckBox(LanguageLoader.getString("Scheduler.monday"));
		tuesdayCheckBox = new javax.swing.JCheckBox(LanguageLoader.getString("Scheduler.tuesday"));
		wednesdayCheckBox = new javax.swing.JCheckBox(LanguageLoader.getString("Scheduler.wednesday"));
		thursdayCheckBox = new javax.swing.JCheckBox(LanguageLoader.getString("Scheduler.thursday"));
		fridayCheckBox = new javax.swing.JCheckBox(LanguageLoader.getString("Scheduler.friday"));
		saturdayCheckBox = new javax.swing.JCheckBox(LanguageLoader.getString("Scheduler.saturday"));
		sundayCheckBox = new javax.swing.JCheckBox(LanguageLoader.getString("Scheduler.sunday"));
		JPanel weekPanel = createFlowLayoutJPanel(mondayCheckBox,tuesdayCheckBox,wednesdayCheckBox,thursdayCheckBox,fridayCheckBox,saturdayCheckBox,sundayCheckBox);
		addCmpToGridBag(weekPanel,new GBC(1,3));
		
		timeLabel = new javax.swing.JLabel(LanguageLoader.getString("Scheduler.time"));
		addCmpToGridBag(timeLabel,new GBC(0,7));
		
		
		hourLabel = new javax.swing.JLabel(LanguageLoader.getString("Scheduler.hour"));
		
		String[] timeData = new String[]{"0","1","2","3","4","5","6","7","8","9","10"
				,"11","12","13","14","15","16","17","18","19","20","21","22","23"};
		timeCombo = new JComboBox(timeData);

		
		minuteLabel = new javax.swing.JLabel(LanguageLoader.getString("Scheduler.minute"));

		minuteField = new javax.swing.JTextField(4);
		minuteField.setInputVerifier(new IntegerVerifier(this, true, 0, 59));
	
		
		secondLabel = new javax.swing.JLabel(LanguageLoader.getString("Scheduler.second"));
		secondField = new javax.swing.JTextField(4);
		secondField.setInputVerifier(new IntegerVerifier(this, true, 0, 59));
		
		JPanel removeLinkPanel = createFlowLayoutJPanel(hourLabel,timeCombo,minuteLabel,minuteField,secondLabel,secondField);
		addCmpToGridBag(removeLinkPanel,new GBC(1,7));
		
		addCmpToGridBag(new JLabel(),new GBC(0,8,2,1));
		
	}
	protected void initActionListener(){
		class CheckBoxActionAdapter implements ActionListener{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent arg0) {
				getWeek();
			}
		}
		mondayCheckBox.addActionListener(new CheckBoxActionAdapter());
		tuesdayCheckBox.addActionListener(new CheckBoxActionAdapter());
		wednesdayCheckBox.addActionListener(new CheckBoxActionAdapter());
		thursdayCheckBox.addActionListener(new CheckBoxActionAdapter());
		fridayCheckBox.addActionListener(new CheckBoxActionAdapter());
		saturdayCheckBox.addActionListener(new CheckBoxActionAdapter());
		sundayCheckBox.addActionListener(new CheckBoxActionAdapter());
	}
	private void getWeek(){
		StringBuilder weekStr = new StringBuilder();
		if(mondayCheckBox.isSelected()){
			weekStr.append("2");
		}
		if(tuesdayCheckBox.isSelected()){
			weekStr.append("3");
		}
		if(wednesdayCheckBox.isSelected()){
			weekStr.append("4");
		}
		if(thursdayCheckBox.isSelected()){
			weekStr.append("5");
		}
		if(fridayCheckBox.isSelected()){
			weekStr.append("6");
		}
		if(saturdayCheckBox.isSelected()){
			weekStr.append("7");
		}
		if(sundayCheckBox.isSelected()){
			weekStr.append("1");
		}
		week = weekStr.toString();
	}
	public void initData(ScheduleTaskBean t){
		if(t == null){
			t = new ScheduleTaskBean();
		}
		fillComponentData(t);
	}
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#fillComponentData(java.lang.Object)
	 */
	@Override
	protected void fillComponentData(ScheduleTaskBean t) {
		List<CrawlerRuleBean> resultList = (List<CrawlerRuleBean>) this.crawlerCacheManager.getValue(CacheKeyConstant.CACHE_KEY_RULE);
		if(CollectionUtils.isEmpty(resultList)){
			resultList = (List<CrawlerRuleBean>)crawlerCacheManager.loadCacheByKey(CacheKeyConstant.CACHE_KEY_RULE);
		}
		taskCombo.setModel(new RuleComboBoxModel(resultList));
		taskCombo.repaint();
		
		if(null != t && null != t.getAssociateId()){
			CrawlerRuleBean crawlerRuleBean = null;
			for(CrawlerRuleBean tempCrawlerRuleBean : resultList){
				if(tempCrawlerRuleBean.getRuleId().intValue() == t.getAssociateId().intValue()){
					crawlerRuleBean = tempCrawlerRuleBean;
					break;
				}
			}
			if(null != crawlerRuleBean){
				taskCombo.setSelectedItem(crawlerRuleBean);
			}else{
				taskCombo.setSelectedItem("");
			}
		}else{
			taskCombo.setSelectedItem("");
		}
		if(StringUtils.isNotBlank(t.getName())){
			nameField.setText(t.getName());
		}else{
			nameField.setText("");
		}
		if(StringUtils.isNotBlank(t.getExpression())){
			expressionField.setText(t.getExpression());
		}else{
			expressionField.setText("");
		}
		if(StringUtils.isNotBlank(t.getWeek())){
			mondayCheckBox.setSelected(t.getWeek().contains("2") ? true : false);
			tuesdayCheckBox.setSelected(t.getWeek().contains("3") ? true : false);
			wednesdayCheckBox.setSelected(t.getWeek().contains("4") ? true : false);
			thursdayCheckBox.setSelected(t.getWeek().contains("5") ? true : false);
			fridayCheckBox.setSelected(t.getWeek().contains("6") ? true : false);
			saturdayCheckBox.setSelected(t.getWeek().contains("7") ? true : false);
			sundayCheckBox.setSelected(t.getWeek().contains("1") ? true : false);
		}else{
			mondayCheckBox.setSelected(false);
			tuesdayCheckBox.setSelected(false);
			wednesdayCheckBox.setSelected(false);
			thursdayCheckBox.setSelected(false);
			fridayCheckBox.setSelected(false);
			saturdayCheckBox.setSelected(false);
			sundayCheckBox.setSelected(false);
		}
		timeCombo.setSelectedItem(t.getHour());
		if(StringUtils.isNotBlank(t.getMinute())){
			minuteField.setText(t.getMinute());
		}else{
			minuteField.setText("");
		}
		if(StringUtils.isNotBlank(t.getSecond())){
			secondField.setText(t.getSecond());
		}else{
			secondField.setText("");
		}
		
	}

}
