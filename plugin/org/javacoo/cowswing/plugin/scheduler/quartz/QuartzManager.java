package org.javacoo.cowswing.plugin.scheduler.quartz;

/**
 * 
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2017年6月12日下午1:04:13
 */
public interface QuartzManager{
	/**
	 * 启动定时任务
	 * <p>说明:</p>
	 * <li></li>
	 * @param scheduleTask
	 * @since 2017年11月30日上午8:49:10
	 */
    void start(org.javacoo.cowswing.plugin.scheduler.domain.Scheduler scheduleTask);
    
   /**
    * 停止任务
    * <p>说明:</p>
    * <li></li>
    * @param scheduleTask
    * @since 2017年11月30日上午8:49:24
    */
	void stop(org.javacoo.cowswing.plugin.scheduler.domain.Scheduler scheduleTask);
   
}
