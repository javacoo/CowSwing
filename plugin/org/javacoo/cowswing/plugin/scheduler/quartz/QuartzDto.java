package org.javacoo.cowswing.plugin.scheduler.quartz;


public class QuartzDto {

	private Long id;
	// 设置trigger名称
	private String triggername;  
	//设置表达式
	private String cronexpression;
	// 设置Job名称
	private String jobdetailname = "detailname";
	//任务类名
	private String targetobject = "com.common.quartz.impl.DefaultLotteryTaskImpl";
	// 是否停用,1:否,0:是
	private String status;
	/**是否运行中:0->否,1->是*/
	private String running;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTriggername() {
		return triggername;
	}
	public void setTriggername(String triggername) {
		this.triggername = triggername;
	}
	public String getCronexpression() {
		return cronexpression;
	}
	public void setCronexpression(String cronexpression) {
		this.cronexpression = cronexpression;
	}
	public String getJobdetailname() {
		return jobdetailname;
	}
	public void setJobdetailname(String jobdetailname) {
		this.jobdetailname = jobdetailname;
	}
	public String getTargetobject() {
		return targetobject;
	}
	public void setTargetobject(String targetobject) {
		this.targetobject = targetobject;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRunning() {
		return running;
	}
	public void setRunning(String running) {
		this.running = running;
	}
	
	

}
