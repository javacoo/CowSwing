package org.javacoo.cowswing.plugin.gather.service.export.builder;


import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.springframework.stereotype.Component;

/**
 * 生成EXCEL
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18下午2:40:35
 * @version 1.0
 */
@Component(GatherConstant.EXPORT_TYPE_EXCEL)
public class ExportExcelBuilder extends AbstractExportBeanBuilder{
	private final static String EXCEL_FILE_EXTENSION = ".xls";
    private final static String EXCEL_CONTENT_TYPE = "application/vnd.ms-excel";
	@Override
	protected void buildExportContentBean() {
		this.exportContentBean.setDownloadType(EXCEL_CONTENT_TYPE);
		this.exportContentBean.setFileName(this.exportParamBean.getFileName() + EXCEL_FILE_EXTENSION);
		this.exportContentBean.setByteContent(getByteContent());
	}
    /**
     * 取得组装好的excel文件 byte[]
     * @return
     */
	private byte[] getByteContent(){
		return this.exportContentBean.getByteContent();
	}
    
	

}
