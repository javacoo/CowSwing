package org.javacoo.cowswing.plugin.gather.service.export;

import java.util.Map;

import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportBean;


/**
 * 文件下载接口
 * <p>
 * 
 * @author DuanYong
 * @since 2012-07-20
 * @version 1.0
 * @updated 2012-07-20
 */
public interface ExportHelper {
	/**
	 * 生成文件下载bean
	 * @param fileType 文件类别
	 * @param fileName 文件名称
	 * @param templateName 模板名称
	 * @param resultMap 结果集
	 * @return 文件下载bean
	 */
	ExportBean generateExportBean(String fileType,
			String fileName,
			String templateName,
			Map<Object,Object> resultMap);

}
