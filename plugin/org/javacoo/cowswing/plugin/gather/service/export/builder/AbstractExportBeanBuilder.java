package org.javacoo.cowswing.plugin.gather.service.export.builder;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportContentBean;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportParamBean;
import org.javacoo.cowswing.plugin.gather.service.export.handler.TemplateHandler;

/**
 * 
 * 文件内容生成抽象类
 * <p>
 * 定义了一些公用方法和主要流程
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18下午2:24:53
 * @version 1.0
 */
public abstract class AbstractExportBeanBuilder implements ExportBeanBuilder {
	protected Log logger = LogFactory.getLog(AbstractExportBeanBuilder.class);
	@Resource(name = "templateHandler")
	private TemplateHandler templateHandler;
	/** 数据导出bean */
	protected ExportContentBean exportContentBean;
	/** 数据导出参数bean */
	protected ExportParamBean exportParamBean;


	public ExportContentBean build(ExportParamBean exportParamBean) {
		doBuild(exportParamBean);
		return this.exportContentBean;
	}

	/**
	 * 组装下载bean，由子类实现
	 */
	protected abstract void buildExportContentBean();

	/**
	 * 初始化
	 * 
	 * @param parameters
	 *            参数bean
	 */
	private void init(ExportParamBean exportParamBean) {
		this.exportParamBean = exportParamBean;
		this.exportContentBean = new ExportContentBean();
	}

	/**
	 * 生成下载bean
	 * 
	 * @param parameters
	 *            参数bean
	 */
	private final void doBuild(ExportParamBean exportParamBean) {
		init(exportParamBean);
		populateTemplate();
		buildExportContentBean();
	}
	
	/**
     * 组装模板
     */
	private void populateTemplate(){
		templateHandler.populateTemplate(this.exportContentBean, this.exportParamBean);
	}

}
