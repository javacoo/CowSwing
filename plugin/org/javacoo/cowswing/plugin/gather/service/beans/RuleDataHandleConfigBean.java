/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.service.beans;

/**
 * 数据处理设置值对象
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-17上午10:31:35
 * @version 1.0
 */
public class RuleDataHandleConfigBean {
	/**是否使用数据导出*/
	private String useExportFlag;
	/**导出数据为列表*/
	private String exportListFlag;
	/**保存路径*/
	private String savePath;
	/**数据导出类型*/
	private String useExportType;
	/**模板名称*/
	private String templateName;
	public String getUseExportFlag() {
		return useExportFlag;
	}
	public void setUseExportFlag(String useExportFlag) {
		this.useExportFlag = useExportFlag;
	}
	
	public String getExportListFlag() {
		return exportListFlag;
	}
	public void setExportListFlag(String exportListFlag) {
		this.exportListFlag = exportListFlag;
	}
	public String getSavePath() {
		return savePath;
	}
	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}
	public String getUseExportType() {
		return useExportType;
	}
	public void setUseExportType(String useExportType) {
		this.useExportType = useExportType;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	
	
	

}
