package org.javacoo.cowswing.plugin.gather.ui.view.panel.rule;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.loader.ImageLoader;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleBean;
import org.javacoo.cowswing.plugin.gather.service.beans.ExtendFieldsBean;
import org.javacoo.cowswing.plugin.gather.service.beans.RuleContentBean;
import org.javacoo.cowswing.plugin.gather.ui.model.CrawlerMidExtendFieldsTabelModel;
import org.javacoo.cowswing.plugin.gather.ui.view.dialog.RuleSettingDialog;
import org.javacoo.cowswing.plugin.gather.ui.view.dialog.TestRuleInfoDialog;
import org.javacoo.cowswing.ui.listener.IntegerVerifier;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;
import org.springframework.stereotype.Component;
/**
 * 内容属性采集规则设置
 *@author DuanYong
 *@since 2012-11-10上午9:55:07
 *@version 1.0
 */
@Component("ruleContentSettingPanel")
public class RuleContentSettingPanel extends AbstractContentPanel<RuleContentBean>{
	private static final long serialVersionUID = 1L;
	/**采集地址输入框*/
	private javax.swing.JTextArea linkListArea;
	/**采集地址标签*/
	private javax.swing.JLabel linkListLabel;
	/**动态连接地址输入框*/
	private javax.swing.JTextField dynamicLinkField;
	/**动态连接地址标签*/
	private javax.swing.JLabel dynamicLinkLabel;
	/**动态连接地址说明标签*/
	private javax.swing.JLabel dynamicLinkDescLabel;
	/**动态页开始输入框*/
	private javax.swing.JTextField dynamicPageStartField;
	/**动态页开始标签*/
	private javax.swing.JLabel dynamicPageStartLabel;
	/**动态页开始结束输入框*/
	private javax.swing.JTextField dynamicPageEndField;
	/**动态页开始结束标签*/
	private javax.swing.JLabel dynamicPageEndLabel;
	/**动态页步长输入框*/
	private javax.swing.JTextField dynamicPageStepField;
	

	/**是否需要动态解析标签*/
	private javax.swing.JLabel dynamicParserLabel;
	/**是否需要动态解析单选按钮*/
	private javax.swing.JRadioButton dynamicParserYesButton;
	/**是否需要动态解析单选按钮*/
	private javax.swing.JRadioButton dynamicParserNoButton;
	/**是否需要动态解析单选按钮组*/
	private javax.swing.ButtonGroup dynamicParserButtonGroup;
	
	
	/**动态页步长标签*/
	private javax.swing.JLabel dynamicPageStepLabel;
	/**连接区域属性输入框*/
	private javax.swing.JTextArea linkIncludeArea;
	/**连接区域属性标签*/
	private javax.swing.JLabel linkIncludeLabel;
	/**连接区域过滤属性输入框*/
	private javax.swing.JTextArea linkFilterArea;
	/**连接区域过滤属性标签*/
	private javax.swing.JLabel linkFilterLabel;
	
	
	/**中间连接扩展字段标签*/
	private javax.swing.JLabel extendsFieldsLabel;
	/**中间连接扩展字段panel*/
	private JComponent extendsFieldsPanel;
	/**添加中间连接扩展字段Button*/
	private JButton addExtednsFieldsButton;
	/**删除中间连接扩展字段Button*/
	private JButton deleteExtednsFieldsButton;
	/**中间连接扩展字段JTable*/
	private JTable crawlerExtendFieldsTabel;
	/**中间连接扩展字段TabelMode*/
	private CrawlerMidExtendFieldsTabelModel dataModel;
	
	
	/**描述区域属性输入框*/
	private javax.swing.JTextArea descIncludeArea;
	/**描述区域属性标签*/
	private javax.swing.JLabel descIncludeLabel;
	/**描述区域过滤属性输入框*/
	private javax.swing.JTextArea descFilterArea;
	/**描述区域过滤属性标签*/
	private javax.swing.JLabel descFilterLabel;
	/**内容区域属性输入框*/
	private javax.swing.JTextArea contentIncludeArea;
	/**内容区域属性标签*/
	private javax.swing.JLabel contentIncludeLabel;
	/**内容区域过滤属性输入框*/
	private javax.swing.JTextArea contentFilterArea;
	/**内容区域过滤属性标签*/
	private javax.swing.JLabel contentFilterLabel;
	/**测试采集规则按钮*/
	private JButton testButton;
	/**测试采集规则信息*/
	@Resource(name="testRuleInfoDialog")
	private TestRuleInfoDialog testRuleInfoDialog;
	@Resource(name="ruleSettingDialog")
	private RuleSettingDialog ruleSettingDialog;
	

	private String dynamicParserSelectValue;
	
	/**
	 * 初始化面板控件
	 */
	protected void initComponents(){
		useGridBagLayout();
		linkListLabel = new javax.swing.JLabel();
		linkListArea = new javax.swing.JTextArea(2,40);

		dynamicLinkLabel = new javax.swing.JLabel();
		dynamicLinkDescLabel = new javax.swing.JLabel();
		dynamicLinkField = new javax.swing.JTextField();
		
		dynamicPageStartLabel = new javax.swing.JLabel();
		dynamicPageStartField = new javax.swing.JTextField();
		
		dynamicPageEndLabel = new javax.swing.JLabel();
		dynamicPageEndField = new javax.swing.JTextField();
		
		dynamicPageStepLabel = new javax.swing.JLabel();
		dynamicPageStepField = new javax.swing.JTextField();
		
		dynamicParserLabel = new javax.swing.JLabel();
		dynamicParserYesButton = new javax.swing.JRadioButton();
		dynamicParserNoButton = new javax.swing.JRadioButton();
		dynamicParserButtonGroup = new javax.swing.ButtonGroup();
		
		
		linkIncludeLabel = new javax.swing.JLabel();
		linkIncludeArea = new javax.swing.JTextArea(3,18);
		
		linkFilterLabel = new javax.swing.JLabel();
		linkFilterArea = new javax.swing.JTextArea();
		
		extendsFieldsLabel = new javax.swing.JLabel();
		
		descIncludeLabel = new javax.swing.JLabel();
		descIncludeArea = new javax.swing.JTextArea(3,18);
		
		descFilterLabel = new javax.swing.JLabel();
		descFilterArea = new javax.swing.JTextArea();
		
		contentIncludeLabel = new javax.swing.JLabel();
		contentIncludeArea = new javax.swing.JTextArea(3,18);
		
		contentFilterLabel = new javax.swing.JLabel();
		contentFilterArea = new javax.swing.JTextArea();
		
		testButton = new JButton(LanguageLoader.getString("RuleContentSetting.testRule"),ImageLoader.getImageIcon("CrawlerResource.zoom"));
		
		
		//添加组件
		
		linkListLabel.setText(LanguageLoader.getString("RuleContentSetting.linkList"));
		addCmpToGridBag(linkListLabel,new GBC(0,0));
		
		JPanel linkListAreaPanel = new JPanel(new BorderLayout()); 
		linkListArea.setLineWrap(true);
		linkListArea.setWrapStyleWord(true);//激活断行不断字功能 
		linkListAreaPanel.add(new JScrollPane(linkListArea));
		addCmpToGridBag(linkListAreaPanel,new GBC(1,0,5,2));
		
		dynamicLinkDescLabel.setText(LanguageLoader.getString("RuleContentSetting.dynamicLinkDesc"));
		addCmpToGridBag(dynamicLinkDescLabel,new GBC(1,2,5,1));
		
		dynamicLinkLabel.setText(LanguageLoader.getString("RuleContentSetting.dynamicLink"));
		addCmpToGridBag(dynamicLinkLabel,new GBC(0,3));

		dynamicLinkField.setColumns(20);
		addCmpToGridBag(dynamicLinkField,new GBC(1,3,5,1));
		
		
		dynamicPageStartLabel.setText(LanguageLoader.getString("RuleContentSetting.dynamicPageStart"));
		addCmpToGridBag(dynamicPageStartLabel,new GBC(0,4));

		dynamicPageStartField.setColumns(5);
		dynamicPageStartField.setInputVerifier(new IntegerVerifier(this, false, 1, 10000));
		
		dynamicPageEndLabel.setText(LanguageLoader.getString("RuleContentSetting.dynamicPageEnd"));
		
		dynamicPageEndField.setColumns(5);
		dynamicPageEndField.setInputVerifier(new IntegerVerifier(this, false, 1, 10000));
	
		dynamicPageStepLabel.setText(LanguageLoader.getString("RuleContentSetting.dynamicPageStep"));
		
		dynamicPageStepField.setColumns(5);
		dynamicPageStepField.setInputVerifier(new IntegerVerifier(this, false, 1, 10000));
		
		dynamicParserLabel.setText(LanguageLoader.getString("RuleContentSetting.dynamicParser"));
		dynamicParserYesButton.setText(LanguageLoader.getString("Common.yes"));
		dynamicParserNoButton.setText(LanguageLoader.getString("Common.no"));
		dynamicParserNoButton.setSelected(true);
		dynamicParserSelectValue = Constant.NO;
		dynamicParserYesButton.setBackground(null);
		dynamicParserNoButton.setBackground(null);
		dynamicParserButtonGroup.add(dynamicParserYesButton);
		dynamicParserButtonGroup.add(dynamicParserNoButton);
		JPanel dynamicParserPanel = createFlowLayoutJPanel(dynamicParserYesButton,dynamicParserNoButton);
		
		JPanel dynamicPagePanel = createFlowLayoutJPanel(dynamicPageStartField,dynamicPageEndLabel,dynamicPageEndField,dynamicPageStepLabel,dynamicPageStepField,dynamicParserLabel,dynamicParserPanel);
		addCmpToGridBag(dynamicPagePanel,new GBC(1,4,5,1));
		
		
		linkIncludeLabel.setText(LanguageLoader.getString("RuleContentSetting.linkInclude"));
		addCmpToGridBag(linkIncludeLabel,new GBC(0,5));

		
		JPanel linkIncludeAreaPanel = new JPanel(new BorderLayout()); 
		linkIncludeArea.setLineWrap(true);
		linkIncludeArea.setWrapStyleWord(true);//激活断行不断字功能 
		linkIncludeAreaPanel.add(new JScrollPane(linkIncludeArea));
		addCmpToGridBag(linkIncludeAreaPanel,new GBC(1,5,2,2));
		
		
		linkFilterLabel.setText(LanguageLoader.getString("RuleContentSetting.linkFilter"));
		addCmpToGridBag(linkFilterLabel,new GBC(3,5));

		JPanel linkFilterAreaPanel = new JPanel(new BorderLayout());
		linkFilterArea.setLineWrap(true);
		linkFilterArea.setWrapStyleWord(true);//激活断行不断字功能 
		linkFilterAreaPanel.add(new JScrollPane(linkFilterArea));
		addCmpToGridBag(linkFilterAreaPanel,new GBC(4,5,2,2));
		
		
		
		extendsFieldsLabel.setText(LanguageLoader.getString("RuleContentSetting.middleLinkFilter"));
		addCmpToGridBag(extendsFieldsLabel,new GBC(0,7));
		
		extendsFieldsPanel = new JPanel(new BorderLayout());
		extendsFieldsPanel.add(getTopButtonPanel(), BorderLayout.NORTH);
		extendsFieldsPanel.add(getExtendsFieldsListPanel(), BorderLayout.CENTER);
		addCmpToGridBag(extendsFieldsPanel,new GBC(1,7,5,3));
		
		descIncludeLabel.setText(LanguageLoader.getString("RuleContentSetting.descInclude"));
		addCmpToGridBag(descIncludeLabel,new GBC(0,12));

		JPanel descIncludeAreaPanel = new JPanel(new BorderLayout());
		descIncludeArea.setLineWrap(true);
		descIncludeArea.setWrapStyleWord(true);//激活断行不断字功能 
		descIncludeAreaPanel.add(new JScrollPane(descIncludeArea));
		addCmpToGridBag(descIncludeAreaPanel,new GBC(1,12,2,1));
		
		
		descFilterLabel.setText(LanguageLoader.getString("RuleContentSetting.descFilter"));
		addCmpToGridBag(descFilterLabel,new GBC(3,12));

		JPanel descFilterAreaPanel = new JPanel(new BorderLayout());
		descFilterArea.setLineWrap(true);
		descFilterArea.setWrapStyleWord(true);//激活断行不断字功能 
		descFilterAreaPanel.add(new JScrollPane(descFilterArea));
		addCmpToGridBag(descFilterAreaPanel,new GBC(5,12,2,1));
		
		
		
		contentIncludeLabel.setText(LanguageLoader.getString("RuleContentSetting.contentInclude"));
		addCmpToGridBag(contentIncludeLabel,new GBC(0,14));

		JPanel contentIncludeAreaPanel = new JPanel(new BorderLayout());
		contentIncludeArea.setLineWrap(true);
		contentIncludeArea.setWrapStyleWord(true);//激活断行不断字功能 
		contentIncludeAreaPanel.add(new JScrollPane(contentIncludeArea));
		addCmpToGridBag(contentIncludeAreaPanel,new GBC(1,14,2,1));
		
		
		contentFilterLabel.setText(LanguageLoader.getString("RuleContentSetting.contentFilter"));
		addCmpToGridBag(contentFilterLabel,new GBC(3,14));

		JPanel contentFilterAreaPanel = new JPanel(new BorderLayout());
		contentFilterArea.setLineWrap(true);
		contentFilterArea.setWrapStyleWord(true);//激活断行不断字功能 
		contentFilterAreaPanel.add(new JScrollPane(contentFilterArea));
		addCmpToGridBag(contentFilterAreaPanel,new GBC(5,14,2,1));

		JPanel testButtonPanel = createFlowLayoutJPanel(testButton);
		addCmpToGridBag(testButtonPanel,new GBC(1,16,5,1));
		
		addCmpToGridBag(new JLabel(),new GBC(0,17,6,1));
	}
	protected JComponent getExtendsFieldsListPanel() {
		return new JScrollPane(getCrawlerExtendFieldsTable());
	}
	/**
	 * <p>方法说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2013-3-17 下午9:08:40
	 * @return
	 * @return Component
	 */ 
	private java.awt.Component getCrawlerExtendFieldsTable() {
		if (crawlerExtendFieldsTabel == null) {
			crawlerExtendFieldsTabel = new JTable();
			dataModel = new CrawlerMidExtendFieldsTabelModel(getColumnNames());
			crawlerExtendFieldsTabel.setModel(dataModel);
			crawlerExtendFieldsTabel.setPreferredScrollableViewportSize(new Dimension(500, 70));
			crawlerExtendFieldsTabel.setFillsViewportHeight(true);

			crawlerExtendFieldsTabel.setAutoCreateRowSorter(true);
		}
		return crawlerExtendFieldsTabel;
	}
	private List<String> getColumnNames() {
		List<String> columnNames = new ArrayList<String>();
		columnNames.add(LanguageLoader.getString("RuleContentSetting.midExtndsFieldName"));
		columnNames.add(LanguageLoader.getString("RuleContentSetting.midExtndsInclude"));
		columnNames.add(LanguageLoader.getString("RuleContentSetting.midExtndsFilter"));
		return columnNames;
	}
	protected java.awt.Component getTopButtonPanel() {
		JPanel topBar = new JPanel();
		FlowLayout layout = new FlowLayout(FlowLayout.LEADING, 3, 2);
		layout.setAlignment(FlowLayout.RIGHT);
		topBar.setLayout(layout);
		class AddExtednsFieldsAction extends AbstractAction{
			private static final long serialVersionUID = 1L;
			public AddExtednsFieldsAction(){
				super(LanguageLoader.getString("RuleContentSetting.addMidLinkFields"),ImageLoader.getImageIcon("CrawlerResource.toolbarRuleAdd"));
			}
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				ExtendFieldsBean extendFieldsBean = new ExtendFieldsBean();
				dataModel.addRow(extendFieldsBean);
			}

		}
		class DeleteExtednsFieldsAction extends AbstractAction{
			private static final long serialVersionUID = 1L;
			public DeleteExtednsFieldsAction(){
				super(LanguageLoader.getString("RuleContentSetting.delMidLinkFields"),ImageLoader.getImageIcon("CrawlerResource.toolbarRuleDelete"));
			}
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				if(crawlerExtendFieldsTabel.getSelectedRow() != -1){
					for(Integer selectRow : crawlerExtendFieldsTabel.getSelectedRows()){
						dataModel.removeRow(selectRow);
					}
				}
			}

		}
		addExtednsFieldsButton = new JButton(new AddExtednsFieldsAction());
		topBar.add(addExtednsFieldsButton);
		deleteExtednsFieldsButton = new JButton(new DeleteExtednsFieldsAction());
		topBar.add(deleteExtednsFieldsButton);
		return topBar;
	}
	protected void initActionListener(){
		testButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(null == ruleSettingDialog.getRuleBaseSettingPanel().getData() ||
				    StringUtils.isBlank(ruleSettingDialog.getRuleBaseSettingPanel().getData().getPageEncoding())){
					JOptionPane.showMessageDialog(null,LanguageLoader.getString("RuleContentSetting.testRuleBaseIsBlank"),
							 LanguageLoader.getString("Common.alertTitle"),
							 JOptionPane.CLOSED_OPTION);
					return;
				}
				CrawlerRuleBean crawlerRuleBean = new CrawlerRuleBean();
				crawlerRuleBean.setRuleBaseBean(ruleSettingDialog.getRuleBaseSettingPanel().getData());
				crawlerRuleBean.setRuleContentBean(populateData());
				if(null == crawlerRuleBean.getRuleContentBean().getAllPlans() || crawlerRuleBean.getRuleContentBean().getAllPlans().length == 0){
					JOptionPane.showMessageDialog(null,LanguageLoader.getString("RuleContentSetting.testRuleUrlIsBlank"),
							 LanguageLoader.getString("Common.alertTitle"),
							 JOptionPane.CLOSED_OPTION);
					return;
				}
				testRuleInfoDialog.init(ruleSettingDialog, Constant.OPTION_TYPE_ADD, LanguageLoader.getString("RuleContentSetting.testRule"));
				testRuleInfoDialog.setCrawlerRuleBean(crawlerRuleBean);
				testRuleInfoDialog.startTest();
				testRuleInfoDialog.setVisible(true);
			}
		});
		
		class DynamicParserBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (dynamicParserYesButton.isSelected()) {
					 	dynamicParserSelectValue = Constant.YES;
	                } else if (dynamicParserNoButton.isSelected()) {
	                	dynamicParserSelectValue = Constant.NO;
	                }
			}
		}
		dynamicParserYesButton.addActionListener(new DynamicParserBtnActionAdapter());
		dynamicParserNoButton.addActionListener(new DynamicParserBtnActionAdapter());
	}
	
	protected void fillComponentData(RuleContentBean ruleContentBean){
		linkListArea.setText(ruleContentBean.getPlanList());
		dynamicLinkField.setText(ruleContentBean.getDynamicAddr());
		dynamicPageStartField.setText(String.valueOf(ruleContentBean.getDynamicStart()));
		dynamicPageEndField.setText(String.valueOf(ruleContentBean.getDynamicEnd()));
		dynamicPageStepField.setText(String.valueOf(ruleContentBean.getStep()));
		if(Constant.YES.equals(ruleContentBean.getDynamicParserFlag())){
			dynamicParserYesButton.setSelected(true);
			dynamicParserSelectValue = Constant.YES;
		}else{
			dynamicParserNoButton.setSelected(true);
			dynamicParserSelectValue = Constant.NO;
		}
		linkIncludeArea.setText(ruleContentBean.getLinksetStart());
		linkFilterArea.setText(ruleContentBean.getLinksetEnd());
		descIncludeArea.setText(ruleContentBean.getDescriptionStart());
		descFilterArea.setText(ruleContentBean.getDescriptionEnd());
		contentIncludeArea.setText(ruleContentBean.getContentStart());
		contentFilterArea.setText(ruleContentBean.getContentEnd());
		if(CollectionUtils.isNotEmpty(ruleContentBean.getMidExtendFields())){
			dataModel.setData(ruleContentBean.getMidExtendFields());
		}else{
			dataModel.setData(new ArrayList<ExtendFieldsBean>());
		}
	}
	

	@Override
	protected RuleContentBean populateData() {
		RuleContentBean ruleContentBean = new RuleContentBean();
		ruleContentBean.setContentEnd(contentFilterArea.getText());
		ruleContentBean.setContentStart(contentIncludeArea.getText());
		ruleContentBean.setDescriptionEnd(descFilterArea.getText());
		ruleContentBean.setDescriptionStart(descIncludeArea.getText());
		ruleContentBean.setDynamicParserFlag(dynamicParserSelectValue);
		ruleContentBean.setDynamicAddr(dynamicLinkField.getText());
		ruleContentBean.setDynamicEnd(Integer.valueOf(dynamicPageEndField.getText()));
		ruleContentBean.setStep(Integer.valueOf(dynamicPageStepField.getText()));
		ruleContentBean.setDynamicStart(Integer.valueOf(dynamicPageStartField.getText()));
		ruleContentBean.setLinksetEnd(linkFilterArea.getText());
		ruleContentBean.setLinksetStart(linkIncludeArea.getText());
		ruleContentBean.setPlanList(linkListArea.getText());
		if(CollectionUtils.isNotEmpty(dataModel.getData())){
			ruleContentBean.setMidExtendFields(dataModel.getData());
		}
		return ruleContentBean;
	}
	public void initData(RuleContentBean t){
		if(t == null){
			t = new RuleContentBean();
		}
		fillComponentData(t);
	}

}
