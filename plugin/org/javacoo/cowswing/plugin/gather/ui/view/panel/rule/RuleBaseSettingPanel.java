package org.javacoo.cowswing.plugin.gather.ui.view.panel.rule;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.commons.lang.StringUtils;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.service.beans.RuleBaseBean;
import org.javacoo.cowswing.ui.listener.IntegerVerifier;
import org.javacoo.cowswing.ui.listener.TextVerifier;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;
import org.springframework.stereotype.Component;
/**
 * 基本参数设置panel
 * 
 * @author javacoo
 * @since 2012-03-14
 */
@Component("ruleBaseSettingPanel")
public class RuleBaseSettingPanel extends AbstractContentPanel<RuleBaseBean>{
	private static final long serialVersionUID = 1L;
	/**采集名称输入框*/
	private javax.swing.JTextField siteField;
	/**采集名称标签*/
	private javax.swing.JLabel siteLabel;
	/**页面编码输入框*/
	private javax.swing.JTextField pageEncodingField;
	/**页面编码标签*/
	private javax.swing.JLabel pageEncodingLabel;
	/**暂停时间输入框*/
	private javax.swing.JTextField sleepTimeField;
	/**暂停时间标签*/
	private javax.swing.JLabel sleepTimeLabel;
	/**是否采集资源单选按钮*/
	private javax.swing.JRadioButton extractResYesButton;
	/**是否采集资源单选按钮*/
	private javax.swing.JRadioButton extractResNoButton;
	/**是否采集资源单选按钮组*/
	private javax.swing.ButtonGroup extractResButtonGroup;
	/**是否采集资源标签*/
	private javax.swing.JLabel extractResLabel;
	/**是否除去连接单选按钮*/
	private javax.swing.JRadioButton removeLinkYesButton;
	/**是否除去连接单选按钮*/
	private javax.swing.JRadioButton removeLinkNoButton;
	/**是否除去连接单选按钮组*/
	private javax.swing.ButtonGroup removeLinkButtonGroup;
	/**是否除去连接标签*/
	private javax.swing.JLabel removeLinkLabel;
	/**是否去重单选按钮*/
	private javax.swing.JRadioButton removeRepeatYesButton;
	/**是否去重单选按钮*/
	private javax.swing.JRadioButton removeRepeatNoButton;
	/**是否去重单选按钮组*/
	private javax.swing.ButtonGroup removeRepeatButtonGroup;
	/**是否去重标签*/
	private javax.swing.JLabel removeRepeatLabel;
	/**是否使用代理单选按钮*/
	private javax.swing.JRadioButton proxyYesButton;
	/**是否使用代理单选按钮*/
	private javax.swing.JRadioButton proxyNoButton;
	/**是否使用代理单选按钮组*/
	private javax.swing.ButtonGroup proxyButtonGroup;
	/**是否使用代理标签*/
	private javax.swing.JLabel proxyLabel;
	
	
	/**采集顺序标签*/
	private javax.swing.JLabel gatherOrderLabel;
	/**采集顺序单选按钮*/
	private javax.swing.JRadioButton gatherOrderYesButton;
	/**采集顺序单选按钮*/
	private javax.swing.JRadioButton gatherOrderNoButton;
	/**采集顺序单选按钮组*/
	private javax.swing.ButtonGroup gatherOrderButtonGroup;
	
	
	
	/**代理地址输入框*/
	private javax.swing.JTextField proxyAddressField;
	/**代理地址标签*/
	private javax.swing.JLabel proxyAddressLabel;
	/**端口入框*/
	private javax.swing.JTextField proxyPortField;
	/**端口标签*/
	private javax.swing.JLabel proxyPortLabel;
	
	/**是否随机日期标签*/
	private javax.swing.JLabel randomDateLabel;
	/**是否随机日期单选按钮*/
	private javax.swing.JRadioButton randomDateYesButton;
	/**是否随机日期单选按钮*/
	private javax.swing.JRadioButton randomDateNoButton;
	/**是否随机日期单选按钮组*/
	private javax.swing.ButtonGroup randomDateButtonGroup;
	/**日期格式标签*/
	private javax.swing.JLabel dateFormatLabel;
	/**日期格式ComboBox*/
	private JComboBox dateFormatComboBox;
	/**开始日期Label*/
	private javax.swing.JLabel startRandomDateLabel;
	/**开始日期Field*/
	private javax.swing.JTextField startRandomDateField;
	/**结束日期Label*/
	private javax.swing.JLabel endRandomDateLabel;
	/**结束日期Field*/
	private javax.swing.JTextField endRandomDateField;
	
	
	/**页面补全URL入框*/
	private javax.swing.JTextField repairPageField;
	/**页面补全URL标签*/
	private javax.swing.JLabel repairPageLabel;
	/**内容页面过滤关键字入框*/
	private javax.swing.JTextField filterKeywordsField;
	/**内容页面过滤关键字标签*/
	private javax.swing.JLabel filterKeywordsLabel;
	/**内容分页补全URL入框*/
	private javax.swing.JTextField repairPaginationField;
	/**内容分页补全URL标签*/
	private javax.swing.JLabel repairPaginationLabel;
	/**替换字符输入框*/
	private javax.swing.JTextArea replaceWordArea;
	/**替换字符标签*/
	private javax.swing.JLabel replaceWordLabel;
	/**采集数量*/
	private javax.swing.JLabel gatherNumLabel;
	/**采集数量入框*/
	private javax.swing.JTextField gatherNumField;
	
	
	
	private String removeLinkValue;
	private String removeRepeatValue;
	private String extractResValue;
	private String proxySelectValue;
	private String randomDateSelectValue;
	private String gatherOrderSelectValue;
	

	/**
	 * 初始化面板控件
	 */
	protected void initComponents(){
		useGridBagLayout();
		//构建组件
		siteLabel = new javax.swing.JLabel();
		siteField = new javax.swing.JTextField();
		
		pageEncodingLabel = new javax.swing.JLabel();
		pageEncodingField = new javax.swing.JTextField();

		sleepTimeLabel = new javax.swing.JLabel();
		sleepTimeField = new javax.swing.JTextField();
		
		extractResLabel = new javax.swing.JLabel();
		extractResYesButton = new javax.swing.JRadioButton();
		extractResNoButton = new javax.swing.JRadioButton();
		extractResButtonGroup = new javax.swing.ButtonGroup();
		
		removeLinkLabel = new javax.swing.JLabel();
		removeLinkYesButton = new javax.swing.JRadioButton();
		removeLinkNoButton = new javax.swing.JRadioButton();
		removeLinkButtonGroup = new javax.swing.ButtonGroup();
		
		removeRepeatLabel = new javax.swing.JLabel();
		removeRepeatYesButton = new javax.swing.JRadioButton();
		removeRepeatNoButton = new javax.swing.JRadioButton();
		removeRepeatButtonGroup = new javax.swing.ButtonGroup();
		
		
		gatherOrderLabel = new javax.swing.JLabel();
		gatherOrderYesButton = new javax.swing.JRadioButton();
		gatherOrderNoButton = new javax.swing.JRadioButton();
		gatherOrderButtonGroup = new javax.swing.ButtonGroup();
		
		proxyLabel = new javax.swing.JLabel();
		proxyYesButton = new javax.swing.JRadioButton();
		proxyNoButton = new javax.swing.JRadioButton();
		proxyButtonGroup = new javax.swing.ButtonGroup();
		
		proxyAddressLabel = new javax.swing.JLabel();
		proxyAddressField = new javax.swing.JTextField();

		proxyPortLabel = new javax.swing.JLabel();
		proxyPortField = new javax.swing.JTextField();
		
		randomDateLabel = new javax.swing.JLabel();
		randomDateYesButton = new javax.swing.JRadioButton();
		randomDateNoButton = new javax.swing.JRadioButton();
		randomDateButtonGroup = new javax.swing.ButtonGroup();
		dateFormatLabel = new javax.swing.JLabel();
		String[] data = new String[]{"yyyy","yyyy-MM","yyyy-MM-dd","yyyy-MM-dd HH:mm:ss.SSS"};
		dateFormatComboBox = new JComboBox(data);
		startRandomDateLabel = new javax.swing.JLabel();
		startRandomDateField = new javax.swing.JTextField();
		endRandomDateLabel = new javax.swing.JLabel();
		endRandomDateField= new javax.swing.JTextField();
		
		
		
		
		
		
		
		repairPageLabel = new javax.swing.JLabel();
		repairPageField = new javax.swing.JTextField();
		
		filterKeywordsLabel = new javax.swing.JLabel();
		filterKeywordsField = new javax.swing.JTextField();
		
		repairPaginationLabel = new javax.swing.JLabel();
		repairPaginationField = new javax.swing.JTextField();
		
		replaceWordLabel = new javax.swing.JLabel();
		replaceWordArea = new javax.swing.JTextArea(4,40);
		
		gatherNumLabel = new javax.swing.JLabel();
		gatherNumField = new javax.swing.JTextField();
		
		
		
		//添加组件
		
		siteLabel.setText(LanguageLoader.getString("RuleContentSetting.name"));
		addCmpToGridBag(siteLabel,new GBC(0,0));

		siteField.setColumns(20);
		siteField.setInputVerifier(new TextVerifier(this,false));
		siteField.setText(LanguageLoader.getString("RuleContentSetting.nameDefaultValue"));
		addCmpToGridBag(siteField,new GBC(1,0,3,1));
		
		sleepTimeLabel.setText(LanguageLoader.getString("RuleContentSetting.sleepTime"));
		addCmpToGridBag(sleepTimeLabel,new GBC(0,1));

		sleepTimeField.setColumns(20);
		sleepTimeField.setInputVerifier(new IntegerVerifier(this, false, 1, 30000));
		addCmpToGridBag(sleepTimeField,new GBC(1,1));
		
		pageEncodingLabel.setText(LanguageLoader.getString("RuleContentSetting.pageEncoding"));
		addCmpToGridBag(pageEncodingLabel,new GBC(2,1));

		pageEncodingField.setColumns(20);
		addCmpToGridBag(pageEncodingField,new GBC(3,1));
		
		
		
		
		removeLinkLabel.setText(LanguageLoader.getString("RuleContentSetting.removeLink"));
		addCmpToGridBag(removeLinkLabel,new GBC(0,2));
		
		removeLinkYesButton.setText(LanguageLoader.getString("Common.yes"));
		removeLinkNoButton.setText(LanguageLoader.getString("Common.no"));
		removeLinkNoButton.setSelected(true);
		removeLinkValue = Constant.NO;
		removeLinkYesButton.setBackground(null);
		removeLinkNoButton.setBackground(null);
		removeLinkButtonGroup.add(removeLinkYesButton);
		removeLinkButtonGroup.add(removeLinkNoButton);
		JPanel removeLinkPanel = createFlowLayoutJPanel(removeLinkYesButton,removeLinkNoButton);
		addCmpToGridBag(removeLinkPanel,new GBC(1,2));
		
		
		extractResLabel.setText(LanguageLoader.getString("RuleContentSetting.extractRes"));
		addCmpToGridBag(extractResLabel,new GBC(2,2));
		extractResYesButton.setText(LanguageLoader.getString("Common.yes"));
		extractResNoButton.setText(LanguageLoader.getString("Common.no"));
		extractResNoButton.setSelected(true);
		extractResValue = Constant.NO;
		extractResYesButton.setBackground(null);
		extractResNoButton.setBackground(null);
		extractResButtonGroup.add(extractResYesButton);
		extractResButtonGroup.add(extractResNoButton);
		extractResYesButton.setBounds(420, 75, 70, 21);
		extractResNoButton.setBounds(500, 75, 70, 21);
		JPanel extractResPanel = createFlowLayoutJPanel(extractResYesButton,extractResNoButton);
		addCmpToGridBag(extractResPanel,new GBC(3,2));
		
		
		removeRepeatLabel.setText(LanguageLoader.getString("RuleContentSetting.removeRepeat"));
		addCmpToGridBag(removeRepeatLabel,new GBC(0,3));
		removeRepeatYesButton.setText(LanguageLoader.getString("Common.yes"));
		removeRepeatNoButton.setText(LanguageLoader.getString("Common.no"));
		removeRepeatNoButton.setSelected(true);
		removeRepeatValue = Constant.NO;
		removeRepeatYesButton.setBackground(null);
		removeRepeatNoButton.setBackground(null);
		removeRepeatButtonGroup.add(removeRepeatYesButton);
		removeRepeatButtonGroup.add(removeRepeatNoButton);
		JPanel removeRepeatPanel = createFlowLayoutJPanel(removeRepeatYesButton,removeRepeatNoButton);
		addCmpToGridBag(removeRepeatPanel,new GBC(1,3));
		
		proxyLabel.setText(LanguageLoader.getString("RuleContentSetting.proxy"));
		addCmpToGridBag(proxyLabel,new GBC(2,3));
		proxyYesButton.setText(LanguageLoader.getString("Common.yes"));
		proxyNoButton.setText(LanguageLoader.getString("Common.no"));
		proxyNoButton.setSelected(true);
		proxySelectValue = Constant.NO;
		proxyYesButton.setBackground(null);
		proxyNoButton.setBackground(null);
		proxyButtonGroup.add(proxyYesButton);
		proxyButtonGroup.add(proxyNoButton);
		JPanel proxyPanel = createFlowLayoutJPanel(proxyYesButton,proxyNoButton);
		addCmpToGridBag(proxyPanel,new GBC(3,3));
		
		
		
		randomDateLabel.setText(LanguageLoader.getString("RuleContentSetting.randomDate"));
		addCmpToGridBag(randomDateLabel,new GBC(0,4));
		randomDateYesButton.setText(LanguageLoader.getString("Common.yes"));
		randomDateNoButton.setText(LanguageLoader.getString("Common.no"));
		randomDateNoButton.setSelected(true);
		randomDateSelectValue = Constant.NO;
		randomDateYesButton.setBackground(null);
		randomDateNoButton.setBackground(null);
		randomDateButtonGroup.add(randomDateYesButton);
		randomDateButtonGroup.add(randomDateNoButton);
		JPanel randomDatePanel = createFlowLayoutJPanel(randomDateYesButton,randomDateNoButton);
		addCmpToGridBag(randomDatePanel,new GBC(1,4));
		
		
		
		
		gatherOrderLabel.setText(LanguageLoader.getString("RuleContentSetting.gatherOrder"));
		addCmpToGridBag(gatherOrderLabel,new GBC(2,4));
		gatherOrderYesButton.setText(LanguageLoader.getString("RuleContentSetting.gatherOrderYes"));
		gatherOrderNoButton.setText(LanguageLoader.getString("RuleContentSetting.gatherOrderNo"));
		gatherOrderNoButton.setSelected(true);
		gatherOrderSelectValue = Constant.NO;
		gatherOrderYesButton.setBackground(null);
		gatherOrderNoButton.setBackground(null);
		gatherOrderButtonGroup.add(gatherOrderYesButton);
		gatherOrderButtonGroup.add(gatherOrderNoButton);
		JPanel gatherOrderPanel = createFlowLayoutJPanel(gatherOrderYesButton,gatherOrderNoButton);
		addCmpToGridBag(gatherOrderPanel,new GBC(3,4));
		
		
		
		
		
		
		proxyAddressLabel.setText(LanguageLoader.getString("RuleContentSetting.proxyAddress"));
		addCmpToGridBag(proxyAddressLabel,new GBC(0,5));

		proxyAddressField.setColumns(20);
		addCmpToGridBag(proxyAddressField,new GBC(1,5));
		
		
		proxyPortLabel.setText(LanguageLoader.getString("RuleContentSetting.proxyPort"));
		addCmpToGridBag(proxyPortLabel,new GBC(2,5));

		proxyPortField.setColumns(20);
		proxyPortField.setInputVerifier(new IntegerVerifier(this, true, 1, 10000));
		addCmpToGridBag(proxyPortField,new GBC(3,5));
		
		
		
		
		
		
		
		
		
		gatherNumLabel.setText(LanguageLoader.getString("RuleContentSetting.gatherNum"));
		addCmpToGridBag(gatherNumLabel,new GBC(0,6));

		gatherNumField.setColumns(20);
		gatherNumField.setInputVerifier(new IntegerVerifier(this, true, 1, 10000));
		addCmpToGridBag(gatherNumField,new GBC(1,6));
		
		
		dateFormatLabel.setText(LanguageLoader.getString("RuleContentSetting.dateFormat"));
		addCmpToGridBag(dateFormatLabel,new GBC(2,6));

		dateFormatComboBox.setBounds(420, 195, 200, 21);
		addCmpToGridBag(dateFormatComboBox,new GBC(3,6));
		
		startRandomDateLabel.setText(LanguageLoader.getString("RuleContentSetting.startRandomDate"));
		addCmpToGridBag(startRandomDateLabel,new GBC(0,7));

		startRandomDateField.setColumns(20);
		addCmpToGridBag(startRandomDateField,new GBC(1,7));
		
		
		endRandomDateLabel.setText(LanguageLoader.getString("RuleContentSetting.endRandomDate"));
		addCmpToGridBag(endRandomDateLabel,new GBC(2,7));

		endRandomDateField.setColumns(20);
		addCmpToGridBag(endRandomDateField,new GBC(3,7));
		
		
		
		
		repairPageLabel.setText(LanguageLoader.getString("RuleContentSetting.repairPage"));
		addCmpToGridBag(repairPageLabel,new GBC(0,8));

		repairPageField.setColumns(20);
		addCmpToGridBag(repairPageField,new GBC(1,8,3,1));
		
		filterKeywordsLabel.setText(LanguageLoader.getString("RuleContentSetting.filterKeywords"));
		addCmpToGridBag(filterKeywordsLabel,new GBC(0,9));

		filterKeywordsField.setColumns(20);
		addCmpToGridBag(filterKeywordsField,new GBC(1,9,3,1));
		
		replaceWordLabel.setText(LanguageLoader.getString("RuleContentSetting.replaceWord"));
		addCmpToGridBag(replaceWordLabel,new GBC(0,10));
		
		JPanel replaceWordAreaPanel = new JPanel(new BorderLayout()); 
		replaceWordArea.setLineWrap(true);
		replaceWordArea.setWrapStyleWord(true);//激活断行不断字功能 
		replaceWordAreaPanel.add(new JScrollPane(replaceWordArea));
		addCmpToGridBag(replaceWordAreaPanel,new GBC(1,10,3,4));
		addCmpToGridBag(new JLabel(),new GBC(0,11,4,1));
	}
	/**
	 * 填充页面控件数据
	 * <p>方法说明:</p>
	 * @auther DuanYong
	 * @since 2012-12-3 上午10:54:08
	 * @param ruleBaseBean
	 * @return void
	 */
	protected void fillComponentData(RuleBaseBean ruleBaseBean){
		logger.info("填充页面控件数据");
		if(StringUtils.isNotBlank(ruleBaseBean.getRuleName())){
			siteField.setText(ruleBaseBean.getRuleName());
		}
		pageEncodingField.setText(ruleBaseBean.getPageEncoding());
		
		sleepTimeField.setText(String.valueOf(ruleBaseBean.getPauseTime()));
		
		if(Constant.YES.equals(ruleBaseBean.getReplaceLinkFlag())){
			removeLinkYesButton.setSelected(true);
			removeLinkValue = Constant.YES;
		}else{
			removeLinkNoButton.setSelected(true);
			removeLinkValue = Constant.NO;
		}
	
		if(Constant.YES.equals(ruleBaseBean.getSaveResourceFlag())){
			extractResYesButton.setSelected(true);
			extractResValue = Constant.YES;
		}else{
			extractResNoButton.setSelected(true);
			extractResValue = Constant.NO;
		}
	
		if(Constant.YES.equals(ruleBaseBean.getRepeatCheckFlag())){
			removeRepeatYesButton.setSelected(true);
			removeRepeatValue = Constant.YES;
		}else{
			removeRepeatNoButton.setSelected(true);
			removeRepeatValue = Constant.NO;
		}
		
		if(Constant.YES.equals(ruleBaseBean.getUseProxyFlag())){
			proxyYesButton.setSelected(true);
			proxySelectValue = Constant.YES;
			changeProxyState(true);
		}else{
			proxyNoButton.setSelected(true);
			proxySelectValue = Constant.NO;
			changeProxyState(false);
		}
		
		if(Constant.YES.equals(ruleBaseBean.getRandomDateFlag())){
			randomDateYesButton.setSelected(true);
			randomDateSelectValue = Constant.YES;
			changeRandomDateState(true);
		}else{
			randomDateNoButton.setSelected(true);
			randomDateSelectValue = Constant.NO;
			changeRandomDateState(false);
		}
		
		if(Constant.YES.equals(ruleBaseBean.getGatherOrderFlag())){
			gatherOrderYesButton.setSelected(true);
			gatherOrderSelectValue = Constant.YES;
		}else{
			gatherOrderNoButton.setSelected(true);
			gatherOrderSelectValue = Constant.NO;
		}
		
		if(StringUtils.isNotBlank(ruleBaseBean.getDateFormat())){
			dateFormatComboBox.setSelectedItem(ruleBaseBean.getDateFormat());
		}
		startRandomDateField.setText(ruleBaseBean.getStartRandomDate());
		
		endRandomDateField.setText(ruleBaseBean.getEndRandomDate());
		
		proxyAddressField.setText(ruleBaseBean.getProxyAddress());
		
		proxyPortField.setText(ruleBaseBean.getProxyPort());
		
		repairPageField.setText(ruleBaseBean.getUrlRepairUrl());
		
		filterKeywordsField.setText(ruleBaseBean.getFilterKeywords());
		
		replaceWordArea.setText(ruleBaseBean.getReplaceWords());
		gatherNumField.setText(ruleBaseBean.getGatherNum());
		
	}
	
	/**
	 * 初始化监听事件
	 * <p>方法说明:</p>
	 * @auther DuanYong
	 * @since 2012-11-16 上午9:36:42
	 * @return void
	 */
	protected void initActionListener(){
		class RemoveLinkBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (removeLinkYesButton.isSelected()) {
					 	removeLinkValue = Constant.YES;
	                } else if (removeLinkNoButton.isSelected()) {
	                	removeLinkValue = Constant.NO;
	                }
			}
		}
		removeLinkYesButton.addActionListener(new RemoveLinkBtnActionAdapter());
		removeLinkNoButton.addActionListener(new RemoveLinkBtnActionAdapter());
		
		class ExtractResBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (extractResYesButton.isSelected()) {
					 	extractResValue = Constant.YES;
	                } else if (extractResNoButton.isSelected()) {
	                	extractResValue = Constant.NO;
	                }
			}
		}
		extractResYesButton.addActionListener(new ExtractResBtnActionAdapter());
		extractResNoButton.addActionListener(new ExtractResBtnActionAdapter());
		
		class RemoveRepeatBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (removeRepeatYesButton.isSelected()) {
					 	removeRepeatValue = Constant.YES;
	                } else if (removeRepeatNoButton.isSelected()) {
	                	removeRepeatValue = Constant.NO;
	                }
			}
		}
		removeRepeatYesButton.addActionListener(new RemoveRepeatBtnActionAdapter());
		removeRepeatNoButton.addActionListener(new RemoveRepeatBtnActionAdapter());
		
		
		class ProxyBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (proxyYesButton.isSelected()) {
					 	proxySelectValue = Constant.YES;
					 	changeProxyState(true);
	                } else if (proxyNoButton.isSelected()) {
	                	proxySelectValue = Constant.NO;
	                	changeProxyState(false);
	                }
			}
		}
		proxyYesButton.addActionListener(new ProxyBtnActionAdapter());
		proxyNoButton.addActionListener(new ProxyBtnActionAdapter());
		
		class RandomBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (randomDateYesButton.isSelected()) {
					 randomDateSelectValue = Constant.YES;
					 changeRandomDateState(true);
	                } else if (randomDateNoButton.isSelected()) {
	                	randomDateSelectValue = Constant.NO;
	                	changeRandomDateState(false);
	                }
			}
		}
		randomDateYesButton.addActionListener(new RandomBtnActionAdapter());
		randomDateNoButton.addActionListener(new RandomBtnActionAdapter());
		
		
		class GatherOrderBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (gatherOrderYesButton.isSelected()) {
					 gatherOrderSelectValue = Constant.YES;
	                } else if (gatherOrderNoButton.isSelected()) {
	                	gatherOrderSelectValue = Constant.NO;
	                }
			}
		}
		gatherOrderYesButton.addActionListener(new GatherOrderBtnActionAdapter());
		gatherOrderNoButton.addActionListener(new GatherOrderBtnActionAdapter());
		
	}
	private void changeProxyState(boolean b){
		proxyAddressField.setEnabled(b);
		proxyAddressLabel.setEnabled(b);
		proxyPortField.setEnabled(b);
		proxyPortLabel.setEnabled(b);
	}
	
	private void changeRandomDateState(boolean b){
		dateFormatLabel.setEnabled(b);
		dateFormatComboBox.setEnabled(b);
		startRandomDateLabel.setEnabled(b);
		startRandomDateField.setEnabled(b);
		endRandomDateLabel.setEnabled(b);
		endRandomDateField.setEnabled(b);
	}
	
	
	@Override
	protected RuleBaseBean populateData() {
		RuleBaseBean ruleBaseBean = new RuleBaseBean();
		ruleBaseBean.setRuleName(siteField.getText());
		ruleBaseBean.setPauseTime(Integer.valueOf(sleepTimeField.getText()));
		ruleBaseBean.setPageEncoding(pageEncodingField.getText());
		ruleBaseBean.setReplaceLinkFlag(this.removeLinkValue);
		ruleBaseBean.setSaveResourceFlag(this.extractResValue);
		ruleBaseBean.setRepeatCheckFlag(this.removeRepeatValue);
		ruleBaseBean.setUseProxyFlag(this.proxySelectValue);
		ruleBaseBean.setGatherOrderFlag(this.gatherOrderSelectValue);
		ruleBaseBean.setProxyAddress(proxyAddressField.getText());
		ruleBaseBean.setProxyPort(proxyPortField.getText());
		ruleBaseBean.setUrlRepairUrl(repairPageField.getText());
		ruleBaseBean.setFilterKeywords(filterKeywordsField.getText());
		ruleBaseBean.setReplaceWords(replaceWordArea.getText());
		ruleBaseBean.setRandomDateFlag(this.randomDateSelectValue);
		if(null != dateFormatComboBox.getSelectedItem()){
			ruleBaseBean.setDateFormat(dateFormatComboBox.getSelectedItem().toString());
		}
		ruleBaseBean.setStartRandomDate(startRandomDateField.getText());
		ruleBaseBean.setEndRandomDate(endRandomDateField.getText());
		ruleBaseBean.setGatherNum(gatherNumField.getText());
		return ruleBaseBean;
	}
	
	public void initData(RuleBaseBean t){
		if(t == null){
			t = new RuleBaseBean();
		}
		fillComponentData(t);
	}
	/**
	 * 完成销毁动作
	 */
	public void dispose(){
		super.dispose();
	}
	
}

