/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.ui.view.panel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerFtpConfigBean;
import org.javacoo.cowswing.ui.listener.IntegerVerifier;
import org.javacoo.cowswing.ui.listener.TextVerifier;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;
import org.springframework.stereotype.Component;


/**
 * FTP参数设置
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2013-4-22 下午5:26:54
 * @version 1.0
 */
@Component("ftpSettingPanel")
public class FtpSettingPanel extends AbstractContentPanel<CrawlerFtpConfigBean>{

	private static final long serialVersionUID = 1L;
	/**容器面板*/
	protected JComponent centerPane;
	/**FTP服务器名称输入框*/
	private javax.swing.JTextField nameField;
	/**FTP服务器名称标签*/
	private javax.swing.JLabel nameLabel;
	/**FTP服务器hostname*/
	private javax.swing.JTextField urlField;
	/**FTP服务器hostname标签*/
	private javax.swing.JLabel urlLabel;
	/**FTP服务器端口输入框*/
	private javax.swing.JTextField portField;
	/**FTP服务器端口标签*/
	private javax.swing.JLabel portLabel;
	/**FTP登录账号输入框*/
	private javax.swing.JTextField userNameField;
	/**FTP登录账号标签*/
	private javax.swing.JLabel userNameLabel;
	/**FTP登录密码输入框*/
	private javax.swing.JTextField passwordField;
	/**FTP登录密码标签*/
	private javax.swing.JLabel passwordLabel;
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#populateData()
	 */
	@Override
	protected CrawlerFtpConfigBean populateData() {
		CrawlerFtpConfigBean crawlerFtpConfigBean = new CrawlerFtpConfigBean();
		crawlerFtpConfigBean.setFtpName(nameField.getText());
		crawlerFtpConfigBean.setFtpPassword(passwordField.getText());
		crawlerFtpConfigBean.setFtpPort(portField.getText());
		crawlerFtpConfigBean.setFtpUrl(urlField.getText());
		crawlerFtpConfigBean.setFtpUserName(userNameField.getText());
		return crawlerFtpConfigBean;
	}

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#initComponents()
	 */
	@Override
	protected void initComponents() {
		this.setLayout(new BorderLayout());
		if(null == centerPane){
			centerPane = new JPanel();
			centerPane.setLayout(new GridBagLayout());
			
			nameLabel = new javax.swing.JLabel();
			nameField = new javax.swing.JTextField();
			
			portLabel = new javax.swing.JLabel();
			portField = new javax.swing.JTextField();
			
			urlLabel = new javax.swing.JLabel();
			urlField = new javax.swing.JTextField();
			
			userNameLabel = new javax.swing.JLabel();
			userNameField = new javax.swing.JTextField();
			
			passwordLabel = new javax.swing.JLabel();
			passwordField = new javax.swing.JTextField();
			
			
			
			nameLabel.setText(LanguageLoader.getString("System.Ftp_name"));
			centerPane.add(nameLabel,new GBC(0,0).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));

			nameField.setColumns(20);
			nameField.setInputVerifier(new TextVerifier(this, false));
			centerPane.add(nameField,new GBC(1,0).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			
			urlLabel.setText(LanguageLoader.getString("System.Ftp_url"));
			centerPane.add(urlLabel,new GBC(0,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));

			urlField.setColumns(20);
			urlField.setInputVerifier(new TextVerifier(this, false));
			centerPane.add(urlField,new GBC(1,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			
			
			portLabel.setText(LanguageLoader.getString("System.Ftp_port"));
			centerPane.add(portLabel,new GBC(0,2).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));

			portField.setColumns(20);
			portField.setInputVerifier(new IntegerVerifier(this, false, 1, 100000));
			centerPane.add(portField,new GBC(1,2).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			
			userNameLabel.setText(LanguageLoader.getString("System.Ftp_userName"));
			centerPane.add(userNameLabel,new GBC(0,3).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));

			userNameField.setColumns(20);
			userNameField.setInputVerifier(new TextVerifier(this, false));
			centerPane.add(userNameField,new GBC(1,3).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			
			passwordLabel.setText(LanguageLoader.getString("System.Ftp_password"));
			centerPane.add(passwordLabel,new GBC(0,4).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));

			passwordField.setColumns(20);
			passwordField.setInputVerifier(new TextVerifier(this, false));
			centerPane.add(passwordField,new GBC(1,4).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			centerPane.add(new JLabel(),new GBC(0,5,2,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 1));
			add(centerPane,BorderLayout.NORTH);
		}
		
		
	}

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#fillComponentData(java.lang.Object)
	 */
	@Override
	protected void fillComponentData(CrawlerFtpConfigBean t) {
		logger.info("填充页面控件数据");
		nameField.setText(t.getFtpName());
		portField.setText(t.getFtpPort());
		urlField.setText(t.getFtpUrl());
		userNameField.setText(t.getFtpUserName());
	}
	
	public void initData(CrawlerFtpConfigBean t){
		if(t == null){
			t = new CrawlerFtpConfigBean();
		}
		fillComponentData(t);
	}

}
