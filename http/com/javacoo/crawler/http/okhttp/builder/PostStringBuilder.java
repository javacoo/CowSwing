package com.javacoo.crawler.http.okhttp.builder;

import com.javacoo.crawler.http.okhttp.request.PostStringRequest;
import com.javacoo.crawler.http.okhttp.request.RequestCall;

import okhttp3.MediaType;
/**
 * 发送文本请求构建器
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月7日下午2:26:23
 */
public class PostStringBuilder extends OkHttpRequestBuilder<PostStringBuilder> {
	private String content;
	private MediaType mediaType;

	public PostStringBuilder content(String content) {
		this.content = content;
		return this;
	}

	public PostStringBuilder mediaType(MediaType mediaType) {
		this.mediaType = mediaType;
		return this;
	}

	@Override
	public RequestCall build() {
		return new PostStringRequest(url, tag, params, headers, content, mediaType, id).build();
	}
}
