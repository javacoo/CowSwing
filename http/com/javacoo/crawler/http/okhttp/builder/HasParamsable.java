package com.javacoo.crawler.http.okhttp.builder;

import java.util.Map;

/**
 * 拥有参数接口
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月7日下午1:37:15
 */
public interface HasParamsable {
	OkHttpRequestBuilder params(Map<String, String> params);
	OkHttpRequestBuilder addParams(String key, String val);
}
