package com.javacoo.crawler.http.okhttp.builder;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.javacoo.crawler.http.okhttp.request.PostFormRequest;
import com.javacoo.crawler.http.okhttp.request.RequestCall;
/**
 * 表单请求构建器
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月7日下午2:24:46
 */
public class PostFormBuilder extends OkHttpRequestBuilder<PostFormBuilder> implements HasParamsable {
	private List<FileInput> files = new ArrayList<>();

	@Override
	public RequestCall build() {
		return new PostFormRequest(url, tag, params, headers, files, id).build();
	}

	public PostFormBuilder files(String key, Map<String, File> files) {
		for (String filename : files.keySet()) {
			this.files.add(new FileInput(key, filename, files.get(filename)));
		}
		return this;
	}

	public PostFormBuilder addFile(String name, String filename, File file) {
		files.add(new FileInput(name, filename, file));
		return this;
	}

	public static class FileInput {
		public String key;
		public String filename;
		public File file;

		public FileInput(String name, String filename, File file) {
			this.key = name;
			this.filename = filename;
			this.file = file;
		}

		@Override
		public String toString() {
			return "FileInput{" + "key='" + key + '\'' + ", filename='" + filename + '\'' + ", file=" + file + '}';
		}
	}

	@Override
	public PostFormBuilder params(Map<String, String> params) {
		this.params = params;
		return this;
	}

	@Override
	public PostFormBuilder addParams(String key, String val) {
		if (this.params == null) {
			params = new LinkedHashMap<>();
		}
		params.put(key, val);
		return this;
	}
}
