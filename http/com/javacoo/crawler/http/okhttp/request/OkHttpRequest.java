package com.javacoo.crawler.http.okhttp.request;

import java.util.Map;
import java.util.Map.Entry;

import com.javacoo.crawler.http.okhttp.callback.Callback;

import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * 抽象请求对象
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * 
 * @author DuanYong
 * @since 2018年2月7日下午1:45:52
 */
public abstract class OkHttpRequest {
	/**请求URL*/
	protected String url;
	/**响应对象*/
	protected Object tag;
	/**请求头信息*/
	protected Map<String, String> headers;
	/**请求参数信息*/
	protected Map<String, String> params;
	/**请求ID*/
	protected int id;

	protected Request.Builder builder = new Request.Builder();

	protected OkHttpRequest(String url, Object tag, Map<String, String> params, Map<String, String> headers, int id) {
		this.url = url;
		this.tag = tag;
		this.params = params;
		this.headers = headers;
		this.id = id;

		if (url == null) {
			throw new IllegalArgumentException("请求URL为空");
		}

		initBuilder();
	}

	/**
	 * 初始化一些基本参数 url , tag , headers
	 */
	private void initBuilder() {
		builder.url(url).tag(tag);
		appendHeaders();
	}

	protected abstract RequestBody buildRequestBody();

	protected RequestBody wrapRequestBody(RequestBody requestBody, final Callback callback) {
		return requestBody;
	}

	protected abstract Request buildRequest(RequestBody requestBody);

	public RequestCall build() {
		return new RequestCall(this);
	}

	public Request generateRequest(Callback callback) {
		RequestBody requestBody = buildRequestBody();
		RequestBody wrappedRequestBody = wrapRequestBody(requestBody, callback);
		Request request = buildRequest(wrappedRequestBody);
		return request;
	}

	protected void appendHeaders() {
		Headers.Builder headerBuilder = new Headers.Builder();
		if (headers == null || headers.isEmpty()){
			return;
		}
		for(Entry<String,String> entry : headers.entrySet()){
			headerBuilder.add(entry.getKey(), entry.getValue());
		}	
		builder.headers(headerBuilder.build());
	}

	public int getId() {
		return id;
	}
}
