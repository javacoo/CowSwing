package com.javacoo.crawler.http;

import java.util.Map;

/**
 * Http头信息
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月1日上午9:57:00
 */
public class HeaderInfo {
	/**
	 * 请求头信息ID
	 */
	private String id;
	/**
	 * 编码
	 */
	private String charset;
	/**
	 * Http头信息Map
	 */
	private Map<String,String> headerMap;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public Map<String, String> getHeaderMap() {
		return headerMap;
	}
	public void setHeaderMap(Map<String, String> headerMap) {
		this.headerMap = headerMap;
	}

	
}
