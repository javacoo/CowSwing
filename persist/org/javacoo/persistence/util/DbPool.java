package org.javacoo.persistence.util;

import java.sql.Connection;
/**
 * 连接池
 * <p>说明:</p>
 * <li></li>
 * @auther DuanYong
 * @since 2016年12月29日上午11:12:37
 */
public interface DbPool {
	/**
	 * 将不再使用的连接返回给连接池
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年12月29日上午11:13:48
	 * @param con
	 */
	void freeConnection(Connection con);
	/**
	 * 从连接池获得一个可用连接.如没有空闲的连接且当前连接数小于最大连接 数限制,则创建新连接.如原来登记为可用的连接不再有效,则从向量删除之,
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年12月29日上午11:13:38
	 * @return
	 */
	Connection getConnection();
	/**
	 * 关闭所有连接
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年12月29日上午11:14:03
	 */
	void release();
	/**
	 * 从连接池获取可用连接.可以指定客户程序能够等待的最长时间 参见前一个getConnection()方法.
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年12月29日上午11:13:13
	 * @param timeout
	 * @return
	 */
	Connection getConnection(long timeout);
}
