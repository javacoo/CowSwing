package org.javacoo.crawler.core.util;

import java.io.File;

import org.javacoo.cowswing.core.constant.Constant;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

/**
 * 动态网站解析工具类
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2017年12月13日下午2:58:10
 */
public class DynamicPageUtil {
	/**
	 * 获取指定URL网页HTML
	 * <p>说明:</p>
	 * <li></li>
	 * @param url
	 * @return
	 * @since 2017年12月13日下午3:26:13
	 */
	public static String getHtml(String url){
		WebDriver driver = createFirefoxDriver("64");
		Point targetPosition = new Point(100000,100000);
		driver.manage().window().setPosition(targetPosition);
		driver.get(url);
		String html = driver.getPageSource();
		close(driver);
		return html;
	}
	/**
	 * 创建火狐浏览器驱动
	 * <p>说明:</p>
	 * <li></li>
	 * @return
	 * @since 2017年12月13日下午3:23:18
	 */
	private static WebDriver createFirefoxDriver(String pos){
		String driverPath = Constant.DRIVER_DIR+"mozilla"+Constant.SYSTEM_SEPARATOR+pos+Constant.SYSTEM_SEPARATOR+"geckodriver.exe";
		File pathToBinary = new File(driverPath);
		System.setProperty("webdriver.gecko.driver", pathToBinary.getAbsolutePath());  
		FirefoxOptions options = new FirefoxOptions();
		FirefoxProfile firefoxProfile = new FirefoxProfile();
		firefoxProfile.setPreference("permissions.default.stylesheet", 2);//去掉css 
		//firefoxProfile.setPreference("permissions.default.image", 2);//去掉图片
		firefoxProfile.setPreference("dom.ipc.plugins.enabled.libflashplayer.so",false);//去掉flash  
		options.setProfile(firefoxProfile);
		return new FirefoxDriver(options);
	}
	/**
	 * 创建Ie浏览器驱动
	 * <p>说明:</p>
	 * <li></li>
	 * @return
	 * @since 2018年1月3日上午10:39:16
	 */
	private static WebDriver createIeDriver(String pos){
		String driverPath = Constant.DRIVER_DIR+"ie"+Constant.SYSTEM_SEPARATOR+pos+Constant.SYSTEM_SEPARATOR+"IEDriverServer.exe";
		File pathToBinary = new File(driverPath);
		System.setProperty("webdriver.ie.driver", pathToBinary.getAbsolutePath());  
		InternetExplorerOptions options = new InternetExplorerOptions();
		return new InternetExplorerDriver();
	}
	/**
	 * 创建Chrome浏览器驱动
	 * <p>说明:</p>
	 * <li></li>
	 * @param pos
	 * @return
	 * @since 2018年1月3日上午10:59:53
	 */
	private static WebDriver createChromeDriver(String pos){
		String driverPath = Constant.DRIVER_DIR+"chrome"+Constant.SYSTEM_SEPARATOR+pos+Constant.SYSTEM_SEPARATOR+"chromedriver.exe";
		File pathToBinary = new File(driverPath);
		System.setProperty("webdriver.chrome.driver", pathToBinary.getAbsolutePath());  
		return new ChromeDriver();
	}
	/**
	 * 关闭浏览器
	 * <p>说明:</p>
	 * <li></li>
	 * @param webDriver
	 * @since 2017年12月13日下午3:24:28
	 */
	private static void close(WebDriver webDriver){
		webDriver.close();
	}

}
