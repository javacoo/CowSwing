package org.javacoo.crawler.core.processor.extractor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.javacoo.crawler.core.constants.Constants;
import org.javacoo.crawler.core.data.Task;
import org.javacoo.crawler.core.data.uri.CrawlResURI;
import org.javacoo.crawler.core.util.HttpUrlHelper;

import com.javacoo.crawler.http.HttpResponse;


/**
 * 任务处理器接口-抽取内容资源实现类
 * @author javacoo
 * @since 2011-11-15
 */
public class ExtractorContentResourceProcessor extends Extractor{
	
	public ExtractorContentResourceProcessor() {
		super();
		setDefaultNextProcessor(new ExtractorFieldProcessor());
	}
	@Override
	protected void extract(Task task) {
		log.info("=========抽取内容资源=========");
		List<CrawlResURI> resCrawlURIList = task.getContentBean().getResCrawlURIList();
		if(task.getController().getCrawlScope().isExtractContentRes() && !CollectionUtils.isEmpty(resCrawlURIList)){
			CrawlResURI parentCrawlResURI = new CrawlResURI();
			parentCrawlResURI.setPort(task.getCrawlURI().getPort());
			parentCrawlResURI.setHost(task.getCrawlURI().getHost());
			parentCrawlResURI.setRawPath(task.getCrawlURI().getRawPath());
			for(CrawlResURI crawlURI : resCrawlURIList){
				crawlURI.setParentURI(parentCrawlResURI);
				String key = crawlURI.getNewResUrl();
				String value = crawlURI.getOriginResUrl();
				if(StringUtils.isNotEmpty(value)){
					HttpResponse rsp = task.getController().getHttpHandler().getSync(HttpUrlHelper.getRawUrl(crawlURI),HttpUrlHelper.getHeaderInfo());
					if (rsp.getContentStream() != null) {
						log.info("=========抽取内容资源->资源类型========="+rsp.getContentType());
						task.getController().getCrawlScope().getFileHelper().saveFile(rsp.getContentStream(), Constants.SYSTEM_ROOT_PATH +key);
					}
				}
			}
		}
		
	}
	

}
