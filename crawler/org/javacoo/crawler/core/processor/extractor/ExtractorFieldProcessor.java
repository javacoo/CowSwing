package org.javacoo.crawler.core.processor.extractor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.javacoo.crawler.core.data.Task;

import com.google.gson.Gson;

/**
 * 任务处理器接口-抽取FIELD内容实现类
 * <li>抽取链 ： 提取内容到指定字段 </li>
 * @author javacoo
 * @since 2011-12-03
 */
public class ExtractorFieldProcessor extends Extractor{

	public ExtractorFieldProcessor() {
		super();
		setDefaultNextProcessor(new ExtractorURIProcessor());
	}

	@Override
	protected void extract(Task task) {
		processorHTML(task);
	}
	
	@SuppressWarnings("unchecked")
	private void processorHTML(Task task){
		log.info("=========提取内容到指定字段=========");
		String extendStr = task.getController().getCrawlScope().getExtendField();
		if(StringUtils.isNotBlank(extendStr)){
			Gson gson = new Gson();
			Map<String,String> extendMap = gson.fromJson(extendStr, Map.class);
			task.getContentBean().getFieldValueMap().putAll(extendMap);
		}
		//如果只抓取列表内容
		if(task.getController().getCrawlScope().isOnlyGatherList()){
			List<String> htmlList = task.getContentBean().getOrginHtmlList();
			List<Map<String,String>> mapList = new ArrayList<>();
			for(String html : htmlList){
				Map<String,String> resultMap = task.getController().getHtmlParserWrapper().getFieldValues(html);
				mapList.add(resultMap);
			}
			task.getContentBean().getFieldValueMapList().addAll(mapList);
		}else{
			String html = task.getContentBean().getOrginHtml();
			Map<String,String> resultMap = task.getController().getHtmlParserWrapper().getFieldValues(html);
			task.getContentBean().getFieldValueMap().putAll(resultMap);
		}
	}
	
}
