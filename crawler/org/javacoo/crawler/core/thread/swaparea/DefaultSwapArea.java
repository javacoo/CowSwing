package org.javacoo.crawler.core.thread.swaparea;

import org.javacoo.crawler.core.data.Task;


/**
 * 默认数据交换区实现
 * <p>说明:</p>
 * <li></li>
 * @auther DuanYong
 * @since 2016年5月15日下午7:19:19
 */
public class DefaultSwapArea implements SwapArea{
    private Task task;
	@Override
	public Task getTask() {
		return task;
	}

	@Override
	public void setTask(Task task) {
		this.task = task;
	}


}
