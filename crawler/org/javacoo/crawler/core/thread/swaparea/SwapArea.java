package org.javacoo.crawler.core.thread.swaparea;

import org.javacoo.crawler.core.data.Task;

/**
 * 内部数据交换区
 * <p>说明:</p>
 * <li></li>
 * @auther DuanYong
 * @since 2016年5月15日下午7:13:43
 */
public interface SwapArea{

	/**
	 * 获取当前任务
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年6月17日上午10:44:33
	 * @return
	 */
	Task getTask();
	/**
	 * 设置任务
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年6月17日上午10:48:19
	 * @param task
	 */
	void setTask(Task task);
}
